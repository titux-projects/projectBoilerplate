<?php

try
{
  $bdd = new PDO('mysql:host=db;dbname=boilerplate', 'boilerplate', 'boilerplate');
}
catch (Exception $e)
{
  die('<br />Erreur : ' . $e->getMessage());
}

echo "<h1>Database is ok!</h1>";

$req = "DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `email` varchar(255) NOT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `users` (`id`, `username`, `email`, `first_name`, `last_name`, `password`, `active`,	`created_at`, `updated_at`)
VALUES
(1,	'Johndoe',	'johndoe@johndoe.com',	'John',	'doe',	'hashpassword',	1,	'1977-08-18 13:37:42',	'1977-08-18 13:37:42'),
(2,	'Test',	'test@test.com',	'Test',	'Test',	'hashpassword',	1,	'1977-08-18 13:37:42',	'1977-08-18 13:37:42');";

$bdd->exec($req);
echo "Users successfully added!\n";

?>