<?php

try
{
  $bdd = new PDO('mysql:host=db;dbname=boilerplate', 'boilerplate', 'boilerplate');
}
catch (Exception $e)
{
  die('<br />Erreur : ' . $e->getMessage());
}

echo "<h1>Database is ok!</h1>";

$q = "SHOW TABLES;";

$res = $bdd->query($q);
$d = $res->fetchAll(PDO::FETCH_OBJ);

foreach($d as $t => $v) {
	foreach($v as $table) {
		echo '<h1>' . ucfirst($table) . '</h1>';
		$req = "SELECT * FROM {$table};";
		$response = $bdd->query($req);
		$data = $response->fetchAll(PDO::FETCH_OBJ);

		echo "<pre>";
		var_dump($data);
		echo "</pre>";
	}
}

phpinfo();

?>